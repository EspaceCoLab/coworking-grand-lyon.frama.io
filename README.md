# Source du site Web du Réseau Coworking Grand Lyon

Bienvenue sur le dépôt du code source du site Web du __Réseau Coworking Grand Lyon__.
Vous êtes au bon endroit si vous souhaitez mettre jour le contenu, la mise en page, etc.
> *Si en revanche vous souhaitez rejoindre le réseaux, contactez-nous : coworking.grandlyon@gmail.com*

Pour participer, commencer par créer un compte compte sur [Framagit](https://framagit.org/users/sign_up).
> __Framagit__ est la forge logicielle hébérgée par __Framasoft__ qui nous permet à notre tour d'hébérger le site du __Réseau Coworking Grand Lyon__.
> Le code est ouvert et tout le monde peut y participer, le partager, etc.

Votre compte sera validé manuellement par un bénévéle de __Framasoft__, soyez patient car cela peut prendre quelques jours.
En attendant, si cela n'est pas déjà fait, vous pouvez [préparer votre fiche espace](#préparer-ma-fiche-dans-le-bac-à-sable).

## Préparer ma fiche dans le bac à sable

1. Créer votre brouillon en cliquant sur le lien suivant : https://pad.georgette.party/new?both
2. Copier le contenu du brouillon d'un autre espace dans le votre pour l'utiliser comme modèle :
- https://pad.georgette.party/coworking-grandlyon-amabla?edit
- https://pad.georgette.party/coworking-grandlyon-cco?edit
- https://pad.georgette.party/dk9AYpQOSLqqao9Ieobm9w?edit
3. Envoyez-nous le lieu de votre brouillon sur la mailing-list, de là, quelqu'un sera certainement en mesure de vous aidez.

## FAQ

- __Je ne suis pas trop *geek* comment ça marche ?__
  Le plus simple reste de vous rapprocher d'un *geek* de votre espace pour vous aidez dans cette tâche !

- __Comment éditer le contenu du site directement ?__
Une fois que vous disposez d'un compte __Framagit__ validé, vous pouvez proposer une modification du contenu directement.
Que ce soit la page d'accueil, la charte ou la page d'espace tout le contenu est accessible dans le dossier `content` :
https://framagit.org/coworking-grand-lyon/coworking-grand-lyon.frama.io/-/tree/master/content
1. Cliquer sur le fichier que vous souhaitez éditer.
2. Puis cliquer sur le bouton bleu __"Ouvrir dans l'EDI Web"__.

## Technologies

- [Hugo]
- This project's static Pages are built by [GitLab CI][ci], following the steps
defined in [`.gitlab-ci.yml`](.gitlab-ci.yml).
- Markdown


## Développement

To work locally with this project, you'll have to follow the steps below:

1. Fork, clone or download this project
1. [Install][] Hugo
1. Preview your project: `hugo server`
1. Add content
1. Generate the website: `hugo` (optional)

Read more at Hugo's [documentation][].

### Preview your site

If you clone or download this project to your local computer and run `hugo server`,
your site can be accessed under `localhost:1313/hugo/`.

The theme used is adapted from http://themes.gohugo.io/beautifulhugo/.

[ci]: https://about.gitlab.com/gitlab-ci/
[hugo]: https://gohugo.io
[install]: https://gohugo.io/overview/installing/
[documentation]: https://gohugo.io/overview/introduction/
[userpages]: http://doc.gitlab.com/ee/pages/README.html#user-or-group-pages
[projpages]: http://doc.gitlab.com/ee/pages/README.html#project-pages
[post]: https://about.gitlab.com/2016/04/07/gitlab-pages-setup/#custom-domains
