---
nom: Melchior Coworking
logo: http://www.melchiorcoworking.fr/images/logo-clubmelchior.png
photo: http://www.melchiorcoworking.fr/files/ext1.jpeg
location: Charly
---

<!-- logo -->
![Melchior Coworking](http://www.melchiorcoworking.fr/images/logo-clubmelchior.png)
<!-- sous-titre -->
Melchior Coworking, un espace de travail partagé dans un cadre exceptionnel de nature et de calme.

# Melchior Coworking

<!-- Photo -->
![](http://www.melchiorcoworking.fr/files/ext1.jpeg)

<!-- Présentation: 1000 caractères max avec les services -->
A 20mn de la place Bellecour, à 14km de Lyon, 350m2 d'espace de travail vous attendent. Au coeur du village de Charly, dans un cadre exceptionnel de verdure et de calme, vous pourrez partager un espace de travail professionnel, coopératif et stimulant.
- une vingtaine de postes de travail
- Fibre
- imprimante
- box téléphone
- 2 salles de réunion
- cuisine et coin détente
- 4 bureaux en location



## Services
- Internet Fibre par câble ethernet ou wifi
- Salle de réunion
- Imprimante / scanner / copieur
- Cuisine, micro-onde, frigidaire
- Coin café/thé
- Accès au parc du domaine ☀️

## Localisation

Au coeur du village de Charly

https://www.melchiorcoworking.fr/
Pour nous contacter : http://www.melchiorcoworking.fr/contact-7
