---
nom: Écoworking
logo: https://www.ecoworking.fr/wp-content/uploads/2021/03/logo_ecowo_rgb_512x512.png
photo: https://www.ecoworking.fr/wp-content/uploads/2021/11/OpenSpace-1er_01-r-scaled.jpg
location: Lyon 1er
---

<!-- logo -->
![Écoworking](https://www.ecoworking.fr/wp-content/uploads/2021/03/logo_ecowo_rgb_512x512.png)
<!-- sous-titre -->
Votre espace de coworking dans l'hypercentre Lyonnais avec une philosophie éco-responsable.
> 🇬🇧 Your Collaborative Working Space in Lyon's hypercenter with an eco-responsible philosophy

# Écoworking

<!-- Photo -->
![](https://www.ecoworking.fr/wp-content/uploads/2021/11/OpenSpace-1er_01-r-scaled.jpg)

<!-- Présentation: 1000 caractères max avec les services -->
Écoworking ouvre ses portes en 2013 afin de proposer un lieu aux professionnels isolés leur permettant de **mutualiser les énergies**, qu’elles soient **matérielles** (locaux, équipements, ressources) ou **humaines** (compétences, connaissances, motivations).

Notre **communauté** est ainsi **bigarrée** : différentes professions, différents statuts, différents savoir-faire… avec un point commun : une envie d’humain, d’échanges et d'ondes positives.

Nous vous proposons la **location de bureaux dédiés**, en open-space à taille humaine (de 4 à 8 bureaux), ou de **salles de réunion**. Nos offres à la **demi-journée**, **journée** ou **au mois** conviennent aussi bien à la personne de passage qu’à celle qui souhaite s’installer pour une durée indéterminée.

## Services
- Internet Haut Débit
- Bureau dédié en open space
- Salles de réunion & Cabines d'appels
- Espace reprographie - A3 & A4
- Domiciliation possible pour nos résidents
- Accès illimité 24/7 pour nos résidents
- Cuisine équipée (plaques de cuisson, four, micro-ondes, frigo)
- [Café](http://cafemokxa.com/) & Thé à volonté
- Parking à vélo 🚲
- Douche
- [Point de collecte Recyclivre](https://www.recyclivre.com/blog/environnement/point-livres/) 📚

## Localisation

Écoworking se situe à côté de la **place des Terreaux**, célèbre place Lyonnaise, **proche de toutes commodités** et **très bien desservie** par les transports en communs (2 Métro, 7 Bus, 4 stations Velov’, 10-20 min des gares Perrache, St Paul et Part Dieu).

Nos locaux se trouvent dans un immeuble *typique des pentes lyonnaises*, à l’abris des regards : ils sont traversant et **donnent sur des cours intérieures** pour un maximum de **lumière et de tranquilité**.

*Profitez ainsi des commodités de l’hypercentre tout en bénéficiant d’un poste de travail au calme !*


[27 rue Romarin, Lyon](https://www.openstreetmap.org/node/2682137336)

https://www.ecoworking.fr/
contact@ecoworking.fr
