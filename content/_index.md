Le coworking peut se définir comme une communauté de personnes et d’organisations qui partagent plus qu’un espace et des outils de travail : des échanges, des liens, des projets, accélérateurs d’innovations.

COWORKING GRAND LYON a pour mission de rassembler la communauté de toutes celles et ceux qui s’impliquent dans le développement du coworking sur le territoire du Grand Lyon.

Au-delà de leur singularité, ces acteurs citoyens se caractérisent par 4 valeurs communes :


- bienveillance
- partage
- égalité
- transparence
